//
//  main.swift
//  infraero
//
//  Created by Aluno on 17/02/2018.
//  Copyright © 2018 Aluno. All rights reserved.
//

import Foundation

// CRIANDO COMPANHIAS
let companhiaTam = Companhia(nome: "TAM")
let companhiaGol = Companhia(nome: "GOL")
let companhiaAzul = Companhia(nome: "Azul")

// CRIANDO VOOS
let vooGoianiaSaoPaulo = Voo(numero: 1515, saida: Date.distantPast, chegada: Date.distantFuture, companhia: companhiaTam)
let vooBrasiliaPalmas = Voo(numero: 7541, saida: Date.distantPast, chegada: Date.distantFuture, companhia: companhiaGol)
let vooFortalezaCuritiba = Voo(numero: 7133, saida: Date.distantPast, chegada: Date.distantFuture, companhia: companhiaAzul)
let vooFortalezaManaus = Voo(numero: 11234, saida: Date.distantPast, chegada: Date.distantFuture, companhia: companhiaTam)

// CRIANDO AEROPORTOS E REGISTRANDO VOOS
let aeroportoGoiania = Aeroporto(nome: "Santa Genoveva")
aeroportoGoiania.adicionarVoo(voo: vooGoianiaSaoPaulo)

let aeroportoBrasilia = Aeroporto(nome: "Mane Garrincha")
aeroportoBrasilia.adicionarVoo(voo: vooBrasiliaPalmas)

let aeroportoFortaleza = Aeroporto(nome: "Sol do meio dia")
aeroportoFortaleza.adicionarVoo(voo: vooFortalezaCuritiba)
aeroportoFortaleza.adicionarVoo(voo: vooFortalezaManaus)

// CRIANDO INFRAERO
let infraero:Infraero = Infraero();
infraero.adicionarAeroporto(aeroporto: aeroportoGoiania)
infraero.adicionarAeroporto(aeroporto: aeroportoBrasilia)
infraero.adicionarAeroporto(aeroporto: aeroportoFortaleza)

var aeroporto:Aeroporto? = infraero.buscarAeroportoPorNome(nome: "Santa Genoveva");
print(aeroporto!.nome)

var voosFortaleza:Array<Voo>? = infraero.buscarVoosAeroportoPorNomeAeroporto(nome: "Sol do meio dia");
if let voos = voosFortaleza {
    for voo in voos {
        print("Numero do voo: \(voo.numero) | Data de saída: \(voo.saida) | Previsão de chegada: \(voo.chegada) | Companhia do Voo: \(voo.companhia.nome)")
    }
}
